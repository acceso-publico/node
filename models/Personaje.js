const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const PersonajeSchema = new Schema({
    id: { type: Number, required: true },
    name: String,
    species: String,
    image: String
})

module.exports = mongoose.model('personaje',PersonajeSchema)