// originalmente users

var express = require('express');
const res=require('express/lib/response')
var router = express.Router();
var PersonajeSchema = require('../controllers/PersonajeController');


/* GET users listing. */

router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});


/* GET home page. */
/*
router.get('/verUsuario', function(req, res, next) {
  res.json({"nombre":"David","apellido":"Blanch"});
});
*/

router.get("/verPersonajes",PersonajeSchema.verPersonajes)
router.get("/favorito",PersonajeSchema.favorito)
router.get("/eliminar/:id",PersonajeSchema.eliminarPersonaje)
router.get("/crear",PersonajeSchema.crearPersonaje)
module.exports = router;
